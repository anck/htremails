<?php
// Modify the value, and write the structure to a file "data_out.json"
//
// Read the file contents into a string variable,
// and parse the string into a data structure

//$columns = array();
if(isset($_POST['index'])) 
{
	$columns = json_decode($_POST['index'],true);
	
	date_default_timezone_set('America/New_York');
}
else
{
	print_r ($_POST);
}


//Check if date is set. If set change the format to j_M_Y to be writtien to the Database.
if(isset($columns['date']))
{
	//var_dump($columns['date']);
	
	
	if(strtotime($columns['date']))
	{
		$today = date('j_M_Y', strtotime($columns['date']));
		//print_r($today);
	}
	else 
	{
		var_dump($columns['date']);
		$error_string ="Date format is incorrect. Please use full month" . $columns['date'];
		
		//change this to ajax call send error message
		header("location: ERROR.php?=" . $error_string);
	}
} 
else 
{
	$today = date("j_M_Y");
	
}

//set loaned resources to 0 if not set.
if(!isset($columns['loaned']))
{
	$columns['loaned'] = 0;
	
}

//set borrowed resources to 0 if not set.
if(!isset($columns['borrowed']))
{
	$columns['borrowed'] = 0;

}

$priority=$columns["priority"];
$hfname=$columns['hfname'];
$hfdate=$columns['hfdate'];
$hflead=$columns['hflead'];
$hfptr=$columns['hfptr'];
$hfhtr=$columns['hfhtr'];
$hfftr=$columns['hfftr'];
$hftype=$columns['hftype'];


$numEntries=sizeof($priority);


$dyndata["Date"] = $today;
$dyndata_default["Date"] = $today;

for ($x = 0; $x < $numEntries; $x++) {
	//$dyndata["HTR"][$x] = array('Priority'=> "{$priority[$x]}",'Hotfix'=> "{$hfname[$x]}","Received"=> "{$hfdate[$x]}","Lead"=> "{$hflead[$x]}","PTR"=> $hfptr[$x], "HTR"=> $hfhtr[$x],"Type"=> "{$hftype[$x]}","Notes"=> "{$hfnotes[$x]}");
	$dyndata['HTR'][$x] = array('Priority'=> "{$priority[$x]}",'Hotfix'=> "{$hfname[$x]}","Received"=> "{$hfdate[$x]}","Lead"=> "{$hflead[$x]}","PTR"=> $hfptr[$x], "HTR"=> $hfhtr[$x],"FTR"=> $hfftr[$x],"Type"=> "{$hftype[$x]}");
	$dyndata_default['HTR'][$x] = array('Priority'=> "{$priority[$x]}",'Hotfix'=> "{$hfname[$x]}","Received"=> "{$hfdate[$x]}","Lead"=> "{$hflead[$x]}","PTR"=> "0", "HTR"=> "0","FTR"=> "0","Type"=> "{$hftype[$x]}");
}



$dyndata["Loaned"] = $columns['loaned'];
$dyndata["Borrowed"] = $columns['borrowed'];

$fh = fopen("htrjsonfiles/HTR_{$today}.json", 'w')
      or die("Error opening output file");
$fhstatic = fopen("htrjsonfiles/HTRjson.json", 'w')
      or die("Error opening output file");

try 
{
	fwrite($fh, json_encode($dyndata, JSON_PRETTY_PRINT));
	fwrite($fhstatic, json_encode($dyndata_default, JSON_PRETTY_PRINT));
	print_r("write complete");
}
catch(Exception $e)
{
	print_r("write incomplete" + $e);
}
fclose($fh);
?>
