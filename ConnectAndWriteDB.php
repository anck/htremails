<?php

require 'MongodbUtility.php';
// Modify the value, and write the structure to a file "data_out.json"
//
// Read the file contents into a string variable,
// and parse the string into a data structure

//$columns = array();
if(isset($_POST['index'])) 
{
	$columns = json_decode($_POST['index'],true);
	date_default_timezone_set('America/New_York');
	
}
else
{
	print_r ($_POST);
}

if(isset($columns['date']))
{
	if(strtotime($columns['date']))
	{
		$today = date('d_M_Y', strtotime($columns['date']));

	}
	else
	{
		$error_string ="Date format is incorrect. Please use full month";
		
		header("location: ERROR.php?=" . $error_string);
	}
}
else
{
	$today = date("d_M_Y");

}

/* else 
{
	header("location: GetHotfix.php");
} */
$priority=$columns["priority"];
$hfname=$columns['hfname'];
$hfdate=$columns['hfdate'];
$hflead=$columns['hflead'];
$hfptr=$columns['hfptr'];
$hfhtr=$columns['hfhtr'];
$hfftr=$columns['hfftr'];
$hftype=$columns['hftype'];
//$hfnotes=$columns['hfnotes'];

$numEntries=sizeof($priority);

$dyndata["Date"] = $today;
for ($x = 0; $x < $numEntries; $x++) {
	$dyndata['HTR'][$x] = array('Priority'=> "{$priority[$x]}",'Hotfix'=> "{$hfname[$x]}","Received"=> "{$hfdate[$x]}","Lead"=> "{$hflead[$x]}","PTR"=> $hfptr[$x], "HTR"=> $hfhtr[$x],"FTR"=> $hfftr[$x],"Type"=> "{$hftype[$x]}");
}

$dyndata["Loaned"] = $columns['loaned'];
$dyndata["Borrowed"] = $columns['borrowed'];
try 
{
	
	//$obj = new MongodbUtility("10.15.196.18", "HTRTestDB", "test2");
	$obj = new MongodbUtility();
	$obj -> connectDB();
	
	//$str_data = file_get_contents("./htrjsonfiles/HTRjson.json");
	//$data = json_decode($str_data,true);
	//var_dump($data);
	//$obj->insertToMongodb($dyndata);
	$final = $obj->findAndModifyToMongodb($dyndata);
	echo $final;
	
}
catch(Exception $e)
{
	print_r("write incomplete  " + $e);
}
/* fclose($fh); */
?>