# README #



###Description\Background:###

Test Leads need to assign tasks for current projects to 3rd party testers. These tasks include but are not limited to testing the package, to check for regressions and to verify fixes. One Test Lead from each team is responsible for coordinating this effort. They send a daily email assigning number of resources to each project with the type of testing.
This information is currently tracked in a spread sheet shared between test lead and the Team Manager. Currently the data, for end of the month reports, is entered manually. Also if we needed to look at the resource allocation statistics it was difficult to lookup. A customized webapp looked like the best approach. The application is lightweight and stores the assignment information in a backend database.

### Problem Statement:

1. Provide a user friendly app to the Test Lead to store assignment data and communicate this to Pactera test leads.

2. Automate monthly reports by storing daily assignments in a database. 


### Scope/Benefit:###

Reduces the need for manually entering and keeping track of data in a spread sheet. Once the data is stored in a database, reports can be generated to slice and dice the data as needed.