<?php
require 'InterfaceDatabase.php';

class MongodbUtility implements InterfaceDatabase
{
	
	private $db;
	private $dbHost;
	private $dbName;
	private $collection;
	private $collectionName;
	private $mongo;
	//counstruct 
	//$dbhost - host IP address
	//$dbname - name of the database
	//$collection - table name
	/* public function MongodbUtility($_dbHost, $_dbName, $_collectionName)
	{
		
		$this->dbHost = $_dbHost;
		$this->dbName = $_dbName;
		$this->collectionName = $_collectionName;
		
	} */
	
	public function MongodbUtility()
	{
		$this->dbHost = "10.108.96.103";
		$this->dbName = "HTRTestDB";
		$this->collectionName = "test2";
	
	}
	
	
	
	
	/**
	 * (non-PHPdoc)
	 * @see InterfaceDatabase::connectDB()
	 */
	function connectDB()
	{
	
		// Configuration
		//$dbhost = "10.108.24.166";
		//echo $this->dbName;
		//echo $this->dbHost;
	
		try
		{
			$this->mongo = new MongoClient("mongodb://$this->dbHost");
			$dbName = $this->dbName;
			//no clue why this is there!
			$this->db = $this->mongo->$dbName;
			
			//var_dump($this->db);
			
			//remove
			//print_r($this->db);
			$this->collection = $this->mongo->selectCollection($this->dbName, $this->collectionName);
			
			//print_r($this->collection);
			
		}
		catch ( MongoConnectionException $e )
		{
			echo '<p>Couldn\'t connect to mongodb, is the "mongo" process running?</p>';
			exit(1);
		}
	}
	
	
	/**
	 * 
	 * 
	 * 
	 */
	function disconnectDB()
	{
	
		try
		{
			$this->mongo->close();
		}
		catch(MongoConnectionException $e)
		{
			echo '<p>Couldn\'t connect to mongodb, is the "mongo" process running?</p>';
			exit(1);
		}
	}
	
	
	//metod to write to db. Pass Json as an array
	public function insertToMongodb($_json)
	{
		
		//$finalJson = json_decode($_json);
		
		$this->collection->insert($_json);
		$this->disconnectDB();
		
	}
	
	//find and update the document
	//http://docs.mongodb.org/manual/reference/method/db.collection.findAndModify/
	//return either Created or Overwrite.
	public function findAndModifyToMongodb($_json)
	{
		//find
		/* db.people.findAndModify({
			query: { name: "Andy" },
			sort: { rating: 1 },
			update: { $inc: { score: 1 } },
			upsert: true
		}) */
		$item = $this->collection->findOne(array( "Date" => $_json["Date"]));
		//print_r($item);
		//print_r($_json);
		if($item != NULL)
		{
			$this->collection->findAndModify(array( "Date" => $_json["Date"]), $_json);
			$this->disconnectDB();
			return "Overwrite";
		}
		else 
		{
			$this->insertToMongodb($_json);
			$this->disconnectDB();
			return "Created";
		}
	}
	
	/**
	 * @param: $query : Read documents from MongoDB  is paramaters passed to MongoDB function
	 * 			<a href="http://docs.mongodb.org/manual/reference/method/db.collection.find/"> Find()</a>
	 * @example: db.bios.find( { Date: 5 } ) == array( "Date" => $_json["Date"])  ; <br />
	 * 			db.products.find( { qty: { $gt: 25 } } )
	 *
	 */
	
	public function findInDBUsingDate($query)
	{
		
		$item = $this->collection->findOne(array( "Date" => $query));
		$this->disconnectDB();
		return $item;
		
	}
	
	
	/**
	 * 
	 * @param unknown $query
	 * @return multitype:multitype: number
	 */
	public function readFromDB($query)
	{
		//print_r ($query);
		$totalResources= array();
		$cursor = $this->collection->find($query);
		//sorting cusrsor on date
		//$cursor = $cursor->sort(array("Date" => -1));
		
		$i = 0;
		//empty query
		if($cursor->hasNext())
		{
			
			$totalPTRPerDoc = array();
			$totalHTRPerDoc = array();
			$totalFTRPerDoc = array();
			
			
			$totalResourcePerTypePerDoc = array();
			$totalResourcePerType= array();
			$datePerDoc = array();
			$loanedResourcesPerDoc = array();
			$borrowedResourcesPerDoc = array();
			$totalLoanedResources = 0;
			$totalBorrowedResources = 0;
			
			//output per Query
			foreach ($cursor as $doc)
			{
				//print_r($doc);
				$totalPTR =0;
				$totalHTR =0;
				$totalFTR =0;
				
				//$totalHTRPerType= array();
				//$totalFTRPerType= array();
				
				//output per doc
				foreach($doc["HTR"] as $hf)
				{
					//output per HF	
					$totalPTR += (int)$hf["PTR"];
					$totalHTR += (int)$hf["HTR"];
					$totalFTR += (int)$hf["FTR"];
					//print_r($hf);
					//Set Keys in the array
					if(!(isset($totalResourcePerType[$hf["Type"]]) || 
							isset($totalResourcePerType[$hf["Type"]]) || 
								isset($totalResourcePerType[$hf["Type"]]) ))
					{
						$totalResourcePerType[$hf["Type"]] = array("PTR" => 0, "HTR" => 0 , "FTR" => 0);
					}
					
					$totalResourcePerType[$hf["Type"]]["PTR"] += (int)$hf["PTR"]; 
					$totalResourcePerType[$hf["Type"]]["HTR"] += (int)$hf["HTR"];
					$totalResourcePerType[$hf["Type"]]["FTR"] += (int)$hf["FTR"];
					
					//$totalHTRPerType[$hf["Type"]] = (int)$hf["HTR"];
					//$totalFTRPerType[$hf["Type"]] = (int)$hf["FTR"];
				}
				
				$totalLoanedResources += (int)$doc["Loaned"]; 
				$totalBorrowedResources += (int)$doc["Borrowed"];
				
				//add values to the different arrays.
				array_push($totalPTRPerDoc,$totalPTR);
				array_push($totalHTRPerDoc,$totalHTR);
				array_push($totalFTRPerDoc,$totalFTR);
				array_push($datePerDoc,$doc["Date"]);
				//array_push($loanedResourcesPerDoc,$doc["Loaned"]);
				//array_push($borrowedResourcesPerDoc,$doc["Borrowed"]);
				//array_push($totalResourcePerTypePerDoc,$totalResourcePerType);
				
				//echo("Per doc Package $totalPTR Regression $totalHTR Fix $totalFTR" );
			}
			
			
			$totalResources["PTR"] = $totalPTRPerDoc;
			$totalResources["HTR"] = $totalHTRPerDoc;
			$totalResources["FTR"] = $totalFTRPerDoc;
			$totalResources["Dates"] = $datePerDoc;
			$totalResources["Type"] = $totalResourcePerType;
			$totalResources["Loaned"] = $totalLoanedResources;
			$totalResources["Borrowed"] = $totalBorrowedResources;
			//$totalResources["HTRType"] = $totalPTRPerType;
			//$totalResources["FTRType"] = $totalPTRPerType;
			//print_r($totalResources);
		} 
		else 
		{
			echo "No Matches Found";	
		}
		//print_r($totalResources);
		$this->disconnectDB();
		return $totalResources;
	
	}
	 
	
	
}

?>
