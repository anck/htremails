<!-- <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<style>
.centerAlign {
  width: 700px ;
  margin-left: auto ;
  margin-right: auto ;
}
input[type="text"]
{
	width: 200px;
}
input[type="number"]
{
	width: 80px;
}

</style>
</head>
<body>--!>

<?php
date_default_timezone_set('America/New_York');
//php to handle total resources. 
if(isset($_POST['loaned']))
{
	$loaned = json_decode($_POST["loaned"],true);
	$borrowed = json_decode($_POST["borrowed"],true);
	$available = json_decode($_POST["available"],true);
	//date_default_timezone_set('America/New_York');
}
else
{
	$loaned = 0;
	$borrowed = 0;
	$available = 19;
}


echo "<br /><div  class=\"centerAlign\">
					<dl class=\"dl-horizontal\">
					<dt style=\"white-space: normal\" >Date: </dt>
					<dd><input type=\"text\" id=\"emaildate\" class=\"form-control\"></dd>
					<dt style=\"white-space: normal\" ></dt><dd><br/></dd>
					<dt style=\"white-space: normal\" >Total Available Resources: </dt>
					<dd><input type=\"number\" class=\"form-control\" id=\"TotalResources\" value=\"$available\" readonly></dd>
					</dl>
					</div>";

echo "<div class=\"centerAlign\" id=\"result\" >
							<dl class=\"dl-horizontal\">
							<dt style=\"white-space: normal\" >Avilable Testers:</dt>
							<dd><input type=\"number\" class=\"form-control\" id=\"TotalAvailableFinal\" readonly></dd>
							</dl>
							</div>
							<hr />";


?>
<!-- begin bootstrap div -->
<div class="container-fluid">

<script>
//Jquery call to set default date for today.
$(document).ready(function() {

    var today = "<?php $datetime = new DateTime(); $datetime->modify('+1 day'); echo $datetime->format('d M Y'); /*echo $datetime->format("j M Y"); /*echo date("d F Y");*/?>";

    $("#emaildate").val(today);
});


//Adds a row form the table
function addRow2(tableID)
{
	var table=document.getElementById(tableID);
	var rowCount=table.rows.length;
	var row=table.insertRow(rowCount);
	var colCount=table.rows[0].cells.length;
	//document.write(colCount);
	for(var i=0;i<colCount;i++)
	{
		var newcell=row.insertCell(i);
		newcell.innerHTML=table.rows[2].cells[i].innerHTML;
		switch(newcell.childNodes[0].type)
		{
			case"text":
				newcell.childNodes[0].value="";
			break;
			case"checkbox":
				newcell.childNodes[0].checked=false;
			break;
			
		}
	}
}

//Deletes a row form the table
function deleteRow(tableID)
	{
		try
		{
			var table=document.getElementById(tableID);
			var rowCount=table.rows.length;
			for(var i=0;i<rowCount;i++)
			{
				var row=table.rows[i];
				var chkbox=row.cells[0].childNodes[0];
				if(null!=chkbox&&true==chkbox.checked)
				{
					table.deleteRow(i);
					rowCount--;
					i--;
				}
			}
		}
		catch(e)
		{alert(e);}
	}


	
//onFocus & blur eventHandler	


//onClick eventhandler for btn1
$(document).ready(function()
{
	$("#btn1").click(function()
	{
		//creating table and enabling related funtionality
		$("#btn3").attr("disabled", false);
		$("#btn4").attr("disabled", false);
		$("#btn5").attr("disabled", false);
		$("#btn6").attr("disabled", false);
		$("span#dropdown").hide();
		//$(".HTRCheckbox").parent().hide();
		$(".HTRCheckbox").closest("td").hide("fast")
		$(".HTRCheckbox").closest("th").hide("fast")
		$(".result").show();
		$(":button").filter("#tablebuttons").attr("disabled", true);
		
		$(".HTRForm").replaceWith(function()
		{
		   return "<span style=\""+ $(this).attr("style") +"\" type=\""+ $(this).attr("type") +"\" class=\"HTRTable\" name=\""+ this.id +"[]\" oninput=\"" + $(this).attr("oninput") + "\" id=\""+ this.id +"\">" + this.value + "</span>";
		});

			
		
	});
});

//onClick event handler for button 2 to generate FORM elements from table elements.
$(document).ready(function()
{
	$("#btn2").click(function()
	{
		//creating form and disabling unrelated funtionality
		$('#btn3').attr("disabled", true);
		$('#btn4').attr("disabled", true);
		$('#btn5').attr("disabled", true);
		$('#btn6').attr("disabled", true);
		//$('.HTRCheckbox').hide(slow);
		$(":button").filter("#tablebuttons").attr("disabled", false);
		$("span#dropdown").show();
		$(".HTRCheckbox").closest("td").show("fast")
		$(".HTRCheckbox").closest("th").show("fast")
		$(".result").hide();
		$(".result").text( "" );
		$(".finalrow").remove();
		$(".HTRTable").replaceWith(function()
		{
			return "<input type=\"" + $(this).attr("type")+ "\" style=\""+ $(this).attr("style") +"\" class=\"HTRForm form-control\" id=\"" + this.id + "\" oninput=\"" + $(this).attr("oninput") + "\" value=\"" + $(this).text() + "\">";
		  // return "<span id=\"HTRTable\">"this.value"</span>;
		});
			
	});
});

//calculate sum for coloums
function columnSum($ColHead)
{
	
	$columnTh = $("#dataTable th:contains(" + $ColHead +")");
	$totalRows = $("#dataTable tr").length;

	$columnIndex = $columnTh.index() + 1; 
	
	$total = 0;
	 
		
	$("#dataTable tr td:nth-child(" + $columnIndex + ")").each(function()
			{
				$total += Number($(this).text());

			});
	
    //$('#dataTable tbody').append('<tr class="child"><td>blahblah</td></tr>');
    
	return $total;
	
}

//load dropdown
$(document).ready(function()
{
	$("select").load("PopulateFileDropDown.php"); 
});

//defunct
//generate total of PTR and HTR coloumns 
$(document).ready(function()
		{
			$("#btn3").click(function()
			{
				var $PTRtotal=0;
				var $HTRtotal=0;
				if($(".HTRTable").length)
				{
					$PTRtotal = columnSum('PTR Resources');
					$HTRtotal = columnSum('HTR Resources');
					$('#dataTable tbody').append("<tr class=\"finalrow\"><td></td><td colspan=\"4\" >TOTAL</td><td>" + $PTRtotal + "</td><td >" + $HTRtotal + "</td><td colspan=\"2\"></td></tr>");
				}
				else if($(".HTRForm").length)
				{
					alert("Please create a table before generating resources");
				}
				$('#btn3').attr("disabled", true);
				
			});
		});


//load form the first time
$(document).ready(function()
{
	//creating form and disabling unrelated funtionality	
	var select = "HTRjson.json";
	$.post("GenerateTable.php", {selection : select }, function(data){ $("#maindiv").html(data);} );
	$('#btn3').attr("disabled", true);
	$('#btn4').attr("disabled", true);
	$('#btn5').attr("disabled", true);
	$('#btn6').attr("disabled", true);
	
});

//reload table
function reloadTable($selectedValue)
{
	var select = $selectedValue;
	$.post("GenerateTable.php", {selection : select }, function(data){ $("#maindiv").html(data);} );
}

//Write JSON
function writeJSON(loaned, borrowed)
{

	$totalRows = $("#dataTable tr").length;
	$htmlIndex = ['priority','hfname','hfdate','hflead','hfptr','hfhtr','hfftr','hftype'];
	var columns = {};
	$index ="";
	
	for($i=0;$i<$htmlIndex.length;$i++)
	{
		$index = $htmlIndex[$i];
		columns[$index] = [];
		//get values from each row.
		for($j=0;$j<$totalRows-1;$j++)
		{
			columns[$index][$j] = document.getElementsByName($index +"[]")[$j].innerHTML;

		}
		
		
		
	}
	columns["date"] = $("#emaildate").val(); 
	console.log(JSON.stringify(columns));
	columns["loaned"] = loaned;
	columns["borrowed"] = borrowed;
	
	$.ajax({
		   type: "POST",
		   data: {index:JSON.stringify(columns)}, 
		   url: "WriteJSON.php",
		   success: function(data){
			   $(".result").html(data);
			   $("select").load("PopulateFileDropDown.php");  
		   }
		});

}

//write to database
function writeDB(loaned, borrowed)
{

	$totalRows = $("#dataTable tr").length;
	$htmlIndex = ['priority','hfname','hfdate','hflead','hfptr','hfhtr','hfftr','hftype'];
	var columns = {};
	$index ="";
	
	for($i=0;$i<$htmlIndex.length;$i++)
	{
		$index = $htmlIndex[$i];
		columns[$index] = [];
		//get values from each row.
		for($j=0;$j<$totalRows-1;$j++)
		{
			columns[$index][$j] = document.getElementsByName($index +"[]")[$j].innerHTML;

		}

	}
	
	columns["date"] = $("#emaildate").val(); 
	console.log(JSON.stringify(columns));
	columns["loaned"] = loaned;
	columns["borrowed"] = borrowed;
	
	$.ajax({
		   type: "POST",
		   data: {index:JSON.stringify(columns)}, 
		   url: "ConnectAndWriteDB.php",
		   success: function(data){
			   $(".result").html("**" + data + "**"); 
		   }
		});
	
}

function writeToJsonAndDb(loaned, borrowed)
{
	writeDB(loaned, borrowed);
	writeJSON(loaned, borrowed);
}



//To use
function tableToJSON( _tablename) 
{
	var tablename = _tablename;
	$totalRows = $("#" + tablename + " tr").length;
	$htmlIndex = ['priority','hfname','hfdate','hflead','hfptr','hfhtr','hfftr','hftype'];
	var columns = {};
	$index ="";
	
	for($i=0;$i<$htmlIndex.length;$i++)
	{
		$index = $htmlIndex[$i];
		columns[$index] = [];
		//get values from each row.
		for($j=0;$j<$totalRows-1;$j++)
		{
			columns[$index][$j] = document.getElementsByName($index +"[]")[$j].innerHTML;

		}

	}
	
	//columns["loaned"] = loaned;
	//columns["borrowed"] = borrowed;	
	console.log(JSON.stringify(columns));
	
	return columns;
}

//handles the modal for sending Email
function emailModalHandler(loaned, borrowed)
{
	dontSendEmail=false;
	emailDate = $("#emaildate").val();
	$("#emailSubject").val("XD HTR Email: " + emailDate);
	$("#emailModal").modal('show');
	//console.log("iam in sendEmail modal");
	$("#emailModalSendmailButton").click(function() {
		
		sendEmail(loaned, borrowed);
});	
}

//generate an email with table as the body.
function sendEmail(loaned, borrowed)
{
	
	//Send data to SeandMail.php
	_json = tableToJSON("dataTable");
	_json["emailFrom"] = $("#emailFrom").val();
	_json["emailTo"] = $("#emailTo").val();
	_json["emailSubject"] = $("#emailSubject").val();
	_json["emailBody"] = $("#emailBody").val().replace(/\r?\n/g, '<br />');
	_json["emailFooter"] = $("#emailFooter").val().replace(/\r?\n/g, '<br />');
	_json["loaned"] = loaned;
	_json["borrowed"] = borrowed;
	//_json = createJSONForEmail("dataTable",loaned, borrowed,emailTo, emailBody );
	console.log(JSON.stringify(_json));
	$.ajax({
		   type: "POST",
		   data: {index:JSON.stringify(_json)}, 
		   url: "SendMail.php",
		   success: function(data){
			   $(".result").html("**" + data + "**"); 
		   }
		});
}




</script>
<?php 

?>

<div class="centerAlign" id="maindiv">

</div>


<span id=dropdown>Choose File To Load  <select  onchange="reloadTable(this.value)"></select></span>
<hr />

<?php


echo "<div class=\"mainbuttondiv centerAlign\" style=\"width: 800px\">";

echo "<input type=\"button\" style= \"border-right: solid \" class=\"btn btn-primary\" value=\"Add Row\" id=\"tablebuttons\" onclick=\"addRow2('dataTable')\" />";
echo "<input type=\"button\" style= \"border-right: solid \" class=\"btn btn-primary\" value=\"Delete Row\" id=\"tablebuttons\" onclick=\"deleteRow('dataTable')\" />";

echo "<button id=\"btn1\" style= \"border-right: solid \" class=\"btn btn-primary\">Lock Table</button>";
echo "<button id=\"btn2\" style= \"border-right: solid \" class=\"btn btn-primary\">Edit Table</button>";

echo "<button id=\"btn4\" style= \"border-right: solid \" class=\"btn btn-success\" onclick=\"writeJSON(". $loaned .",". $borrowed .")\">Write Json</button>";
echo "<button id=\"btn5\" style= \"border-right: solid \" class=\"btn btn-success\" onclick=\"writeDB(". $loaned .",". $borrowed .")\">Write To DataBase</button>";

echo "<button id=\"btn6\" class=\"btn btn-primary\" onclick=\"emailModalHandler(". $loaned .",". $borrowed .")\">Send Email</button>";

echo "</div>";
echo "<div class=\"result\"> </div>";
?>



</div>

<div id="emailModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="dontSendEmail=true">&times;</button>
        <h4 class="modal-title"> <b>HTR Email </b></h4>
      </div>
      <div class="modal-body">
        <p class="bg-danger">
        From:
        <br />
        <input id="emailFrom" value="anchit.kalra@citrix.com">
        <- Please fill in your email address.</p> 
        <br />
        To:
        <br />
        <textarea name="textarea" id="emailTo" rows="2" cols="100">Yujie.Shen@citrix.com,yujie.shen@pactera.com,anchit.kalra@citrix.com, Joseph.Wu@citrix.com, Maheedhar.Gopireddy@citrix.com, Roman.Siryk@citrix.com, Ryan.Beuler@citrix.com</textarea>
        <br />
        <hr />
        Subject: 
        <br />
        <textarea id="emailSubject" rows="1" cols="50" ></textarea> 
        <br />
        <br />
        Body: 
        <br />
        <textarea id="emailBody" rows="4" cols="100">
Hi Yujie,
Following are today's assignments.
         
        </textarea>
        <br />
        <hr />
        Footer -
        <br />
        <textarea id="emailFooter" rows="2" cols="50">
Thanks, 
<your name>
http://10.108.96.103/AssignResources/
</textarea> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="dontSendEmail=true" id="emailModalCloseButton">Close</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="emailModalSendmailButton">Send Email</button>
      </div>
    </div>

  </div>
</div>


<!--</body>
</html> -->
