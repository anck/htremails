<?php require_once 'MongodbUtility.php';
?>
<style>
/* input[type="text"] */
/* { */
/* 	width: 185px; */
/* } */
/* input[type="number"] */
/* { */
/* 	width: 80px; */
/* } */
/* #hfhtr, #hfptr, #priority, #hfftr, #chk */
/* { */
/* 	width: 70px; */
/* } */
</style>
<script type="text/javascript">
<!--

//-->

$(document).ready(function()
		{
			
				//$("span#dropdown").hide();
				//$(".HTRCheckbox").parent().hide();
				$(".HTRCheckbox").closest("td").hide("fast")
				$(".HTRCheckbox").closest("th").hide("fast")
				$(".result").show();
				$(":button").filter("#tablebuttons").attr("disabled", true);
				
				$(".HTRForm").replaceWith(function()
				{
				   return "<span style=\""+ $(this).attr("style") +"\" type=\""+ $(this).attr("type") +"\" class=\"HTRTable\" name=\""+ this.id +"[]\" oninput=\"" + $(this).attr("oninput") + "\" id=\""+ this.id +"\">" + this.value + "</span>";
				});

		});

</script>
<?php

if(isset($_POST['selection'])) 
{
	$requestedDate = $_POST['selection'];
	
	try
	{
	
		//$obj = new MongodbUtility("10.15.196.18", "HTRTestDB", "test2");
		$obj = new MongodbUtility();
		$obj -> connectDB();
	
		//$str_data = file_get_contents("./htrjsonfiles/HTRjson.json");
		//$data = json_decode($str_data,true);
		//var_dump($data);
		//$obj->insertToMongodb($dyndata);
		//db.test.find(  )
		//$query = array( "Date" => $requestedDate);
		//var_dump($requestedDate);
		$final = $obj->findInDBUsingDate($requestedDate);
		
		
	
	}
	catch(Exception $e)
	{
		print_r("write incomplete  " + $e);
	}
	
	//$str_data = file_get_contents("./htrjsonfiles/$fileName");
	$data = $final;
}
else 
{
	header("location: GetHotfix.php");
}

//print_r($data);


echo "<table class=\"tg table table-bordered table-hover \" id=\"dataTable\">
<tr>

<th>Priority</th>
<th>Package Name</th>
<th>Received Date</th>
<th>Lead</th>
<th>Package Resources</th>
<th>Regression Resources</th>
<th>Fix Resources</th>						
<th>Type</th>
<!--<th>Notes</th>-->
</tr>";



$HTR = $data['HTR'];

foreach($HTR as $HF)
{
	echo "<tr><td style=\"width: auto\"><input class=\"HTRCheckbox form-control\" id=\"chk\" type=\"checkbox\" name=\"chk[]\"></td>";
	foreach($HF as $k => $v)
	{
		
		switch($k)
		{
			case "Priority":
				
				echo "<td style=\"width: auto\" ><input type=\"number\" class=\"HTRForm form-control\" name=\"priority[]\" id=\"priority\" value=\"$v\"></td>";
				break;
			case "Hotfix":
				
				echo "<td style=\"width: auto\" ><input style=\"width: 250px\" type=\"text\" class=\"HTRForm form-control\" name=\"hfname[]\" id=\"hfname\"  value=\"$v\"></td>";
				break;
			case "Received":
				
				echo "<td style=\"width: auto\" ><input style=\"width: 100px\" type=\"text\" class=\"HTRForm form-control\" name=\"hfdate[]\" id=\"hfdate\" value=\"$v\"></td>";
				break;
			case "Lead":
				
				echo "<td style=\"width: auto\" ><input style=\"width: 120px\" type=\"text\" class=\"HTRForm form-control\" name=\"hflead[]\" id=\"hflead\" value=\"$v\"></td>";
				break;
			case "PTR":
				
				echo "<td style=\"width: auto\" ><input type=\"number\" class=\"HTRForm form-control\" name=\"hfptr[]\" id=\"hfptr\" oninput=\"calculateAvilableResources()\" value=\"$v\"></td>";
				break;
			case "HTR":
				
				echo "<td style=\"width: auto\" ><input type=\"number\" class=\"HTRForm form-control\" name=\"hfhtr[]\" id=\"hfhtr\" oninput=\"calculateAvilableResources()\" value=\"$v\"></td>";
				break;
			case "FTR":
			
				echo "<td style=\"width: auto\" ><input type=\"number\" class=\"HTRForm form-control\" name=\"hfftr[]\" id=\"hfftr\" oninput=\"calculateAvilableResources()\" value=\"$v\"></td>";
				break;
			case "Type":
				//3/20/15- remove - this is crap! Need to change but too lazy.
				echo "<td><input style=\"width: 120px\" type=\"text\" class=\"HTRForm form-control\" name=\"hftype[]\" id=\"hftype\" value=\"$v\"></td>";
				//echo "<td><input type=\"text\" class=\"HTRForm\" name=\"hfnotes[]\" id=\"hfnotes\" value=\"NA\"></td>\n</tr>";
				break;
			default:
				break;
	
		}
	}
	
}


echo "</table>";
//populate available resources by default.
//echo "<script>calculateAvilableResources();</script>"



?>