<html>

<body>
<?php
ini_set('display_errors', '0');     # don't show any errors...
error_reporting(E_ALL | E_STRICT);  # ...but do log them
require_once "Mail.php";

if(isset($_POST['index']))
{
	$columns = json_decode($_POST['index'],true);
	date_default_timezone_set('America/New_York');

}
else
{
	print_r ($_POST);
}

if(isset($columns['date']))
{
	if(strtotime($columns['date']))
	{
		$today = date('d_M_Y', strtotime($columns['date']));

	}
	else
	{
		$error_string ="Date format is incorrect. Please use full month";
		header("location: ERROR.php?=" . $error_string);
	}
}
else
{
	$today = date("d_M_Y");

}

$priority=$columns["priority"];
$hfname=$columns['hfname'];
$hfdate=$columns['hfdate'];
$hflead=$columns['hflead'];
$hfptr=$columns['hfptr'];
$hfhtr=$columns['hfhtr'];
$hfftr=$columns['hfftr'];
$hftype=$columns['hftype'];
$emailFrom = $columns['emailFrom'];
$emailTo = $columns['emailTo'];
$emailBody = $columns['emailBody'];
$emailSubject = $columns['emailSubject'];
$emailFooter = $columns['emailFooter'];
//$hfnotes=$columns['hfnotes'];

$numEntries=sizeof($priority);

$dyndata["Date"] = $today;
for ($x = 0; $x < $numEntries; $x++) 
{
	$dyndata['HTR'][$x] = array('Priority'=> "{$priority[$x]}",'Hotfix'=> "{$hfname[$x]}","Received"=> "{$hfdate[$x]}","Lead"=> "{$hflead[$x]}","PTR"=> $hfptr[$x], "HTR"=> $hfhtr[$x],"FTR"=> $hfftr[$x],"Type"=> "{$hftype[$x]}");
}

$dyndata["Loaned"] = $columns['loaned'];
$dyndata["Borrowed"] = $columns['borrowed'];

$HTR = $dyndata['HTR'];


//Creating message for Email
$message="<html><body> <p> $emailBody </p>";
$message .= "Total resources loaned = " . $dyndata["Loaned"];
$message .= "<br />Total resources borrowed = " . $dyndata["Borrowed"];
$message .= "<hr /><table border=\"1\">
<tr style= \'border : double text-align: center\'>
<th>Priority</th>
<th>Package Name</th>
<th>Received Date</th>
<th>Lead</th>
<th>Package Resources</th>
<th>Regression Resources</th>
<th>Fix Resources</th>
<th>Type</th>
<!--<th>Notes</th>-->
</tr>";





foreach($HTR as $HF)
{
	$message .= "<tr style= \'border : double text-align: center\'>";
	foreach($HF as $k => $v)
	{

		switch($k)
		{
			case "Priority":

				$message .= "<td style=\"border:solid windowtext 1.0pt;padding:.75pt .75pt .75pt .75pt\" >$v</td>";
				break;
			case "Hotfix":

				$message .= "<td style=\"border:solid windowtext 1.0pt;padding:.75pt .75pt .75pt .75pt\">$v</td>";
				break;
			case "Received":

				$message .= "<td style=\"border:solid windowtext 1.0pt;padding:.75pt .75pt .75pt .75pt\">$v</td>";
				break;
			case "Lead":

				$message .= "<td style=\"border:solid windowtext 1.0pt;padding:.75pt .75pt .75pt .75pt\">$v</td>";
				break;
			case "PTR":

				$message .= "<td style=\"border:solid windowtext 1.0pt;padding:.75pt .75pt .75pt .75pt\">$v</td>";
				break;
			case "HTR":

				$message .= "<td style=\"border:solid windowtext 1.0pt;padding:.75pt .75pt .75pt .75pt\">$v</td>";
				break;
			case "FTR":
					
				$message .= "<td style=\"border:solid windowtext 1.0pt;padding:.75pt .75pt .75pt .75pt\">$v</td>";
				break;
			case "Type":
				//3/20/15- remove - this is crap! Need to change but too lazy.
				$message .= "<td style=\"border:solid windowtext 1.0pt;padding:.75pt .75pt .75pt .75pt\">$v</td>";
				//echo "<td><input type=\"text\" class=\"HTRForm\" name=\"hfnotes[]\" id=\"hfnotes\" value=\"NA\"></td>\n</tr>";
				break;
			default:
				break;

		}
	}
	$message .= "</tr>";
}


$message .= "</table>";
$message .= "<hr /> $emailFooter";
$message .= "</body></html>";


//$from = "Anchit Kalra <anchit.kalra@citrix.com>";
$body = "This is a test email message";
$host = "mail.citrix.com";


$headers = array (
	'MIME-Version' => '1.0',
	'Content-type' => 'text/html; charset=iso-8859-1',
	'From' => $emailFrom,
	'To' => $emailTo,
	'Subject' => $emailSubject);
$smtp = Mail::factory('smtp',
  array ('host' => $host,
    'auth' => false));
 
$mail = $smtp->send($emailTo, $headers, $message);
 
if (PEAR::isError($mail)) 
{
  echo("<p>" . $mail->getMessage() . "</p>");
} 
else 
{
  echo("<p>Message successfully sent!</p>");
}
?>
</body>
</html>