<style>
input[type="text"]
{
	width: 185px;
}
input[type="number"]
{
	width: 80px;
}
#hfhtr, #hfptr, #priority, #hfftr, #chk
{
	width: 70px;
}
</style>
<script>
//calculates resources when PTR or HTR is changed.
function calculateAvilableResources()
{
		
			var totalAssigned = [];
			var totalResources = $("input#TotalResources").val();
			console.log(totalResources);
			//adding the resources to array.
			$("input#hfptr").each(function()
			{
				totalAssigned.push(Number($(this).val()));
			});
			$("input#hfhtr").each(function()
			{
				totalAssigned.push(Number($(this).val()));
			});
			$("input#hfftr").each(function()
			{
				totalAssigned.push(Number($(this).val()));
			});
			
			
			var total =0;
			for(i=0;i<totalAssigned.length;i++)
			{
				total = total + totalAssigned[i];
			}
			
			$('input#TotalAvailableFinal').val(totalResources - total);
}


</script>
<?php

if(isset($_POST['selection'])) 
{
	$fileName = $_POST['selection'];
	$str_data = file_get_contents("./htrjsonfiles/$fileName");
	$data = json_decode($str_data,true);
}
else 
{
	header("location: GetHotfix.php");
}

//print_r($data);


echo "<table class=\"tg table table-bordered table-hover \" id=\"dataTable\">
<tr>
<th class=\"HTRCheckbox\" >Checkbox</th>
<th>Priority</th>
<th>Package Name</th>
<th>Received Date</th>
<th>Lead</th>
<th>Package Resources</th>
<th>Regression Resources</th>
<th>Fix Resources</th>						
<th>Type</th>
<!--<th>Notes</th>-->
</tr>";



$HTR = $data['HTR'];

foreach($HTR as $HF)
{
	echo "<tr><td style=\"width: auto\"><input class=\"HTRCheckbox form-control\" id=\"chk\" type=\"checkbox\" name=\"chk[]\"></td>";
	foreach($HF as $k => $v)
	{
		
		switch($k)
		{
			case "Priority":
				
				echo "<td style=\"width: auto\" ><input type=\"number\" class=\"HTRForm form-control\" name=\"priority[]\" id=\"priority\" value=\"$v\"></td>";
				break;
			case "Hotfix":
				
				echo "<td style=\"width: auto\" ><input style=\"width: 250px\" type=\"text\" class=\"HTRForm form-control\" name=\"hfname[]\" id=\"hfname\"  value=\"$v\"></td>";
				break;
			case "Received":
				
				echo "<td style=\"width: auto\" ><input style=\"width: 100px\" type=\"text\" class=\"HTRForm form-control\" name=\"hfdate[]\" id=\"hfdate\" value=\"$v\"></td>";
				break;
			case "Lead":
				
				echo "<td style=\"width: auto\" ><input style=\"width: 120px\" type=\"text\" class=\"HTRForm form-control\" name=\"hflead[]\" id=\"hflead\" value=\"$v\"></td>";
				break;
			case "PTR":
				
				echo "<td style=\"width: auto\" ><input type=\"number\" class=\"HTRForm form-control\" name=\"hfptr[]\" id=\"hfptr\" oninput=\"calculateAvilableResources()\" value=\"$v\"></td>";
				break;
			case "HTR":
				
				echo "<td style=\"width: auto\" ><input type=\"number\" class=\"HTRForm form-control\" name=\"hfhtr[]\" id=\"hfhtr\" oninput=\"calculateAvilableResources()\" value=\"$v\"></td>";
				break;
			case "FTR":
			
				echo "<td style=\"width: auto\" ><input type=\"number\" class=\"HTRForm form-control\" name=\"hfftr[]\" id=\"hfftr\" oninput=\"calculateAvilableResources()\" value=\"$v\"></td>";
				break;
			case "Type":
				//3/20/15- remove - this is crap! Need to change but too lazy.
				echo "<td><input style=\"width: 120px\" type=\"text\" class=\"HTRForm form-control\" name=\"hftype[]\" id=\"hftype\" value=\"$v\"></td>";
				//echo "<td><input type=\"text\" class=\"HTRForm\" name=\"hfnotes[]\" id=\"hfnotes\" value=\"NA\"></td>\n</tr>";
				break;
			default:
				break;
	
		}
	}
	
}


echo "</table>";
//populate available resources by default.
echo "<script>calculateAvilableResources();</script>"



?>