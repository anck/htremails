<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="jqueryUtility.js"></script>
<style>
 .centerAlign {
  width: 700px ;
  margin-left: auto ;
  margin-right: auto ;
}

.setWidth {
  width: 700px;
  padding-left: 150px;
  padding-right: 150px;
  padding-bottom: 150px; 
}

input[type="text"]
{
	width: 200px;
}
input[type="number"]
{
	width: 80px;
}
</style>
<title>One Pactera Assisgnment</title>
</head>
<body>

<hr />

<script type="text/javascript">

function callReport()
{
	$.ajax({
		   type: "POST",
		   data: {resources: "fuckit"}, 
		   url: "Report.php",
		   success: function(data){
			   $("#contentReport").html(data); 
		   }
		});
	$("#questions").hide("slow");
	$("#gethotfixDiv").hide("slow");
}

function callGetHotfix()
{
	$("#report").hide("slow");
	$.ajax({
		   type: "POST",
		   data: {loaned: $("#TotalLoaned").val(), borrowed: $("#TotalBorrowed").val(), available: $("#TotalAvailabe").val()}, 
		   url: "GetHotfix.php",
		   success: function(data){
			   $("#contentHotfix").html(data); 
		   }
		});
	$("#questions").hide("slow");
	$("#getreportDiv").hide("slow");
}

function refreshIndex()
{
	location.reload();
}


//calculates resources when PTR or HTR is changed.
function calculateAvilableResources()
{
		
				var totalAssigned = [];
				var totalResources = Number($("input#TotalAvailableResources").val());

				//marking loaned resources to a negative value
				$("input#TotalLoaned").each(function()
				{	
					var val = Number($(this).val());
					val *= -1;
					totalAssigned.push(val);
				});
				$("input#TotalBorrowed").each(function()
				{
					totalAssigned.push(Number($(this).val()));
				});
				
				
				var total = 0;
				for(i=0;i<totalAssigned.length;i++)
				{
					total = total + totalAssigned[i];
				}
				
				
				$('input#TotalAvailabe').val(totalResources + total);
				 
			
}


function showAbout()
{

	$("#report").hide("slow");
	$("#questions").hide("slow");
	$("#getreportDiv").hide("slow");
	$("#gethotfixDiv").hide("slow");
	$("#contentReport").hide("quick");
	$("#contentHotfix").hide("quick");
	$.ajax({
		   type: "POST",
		   data: {}, 
		   url: "about.html",
		   success: function(data){
			   $("#contentAbout").html(data); 
		   }
		});
	
	
}

</script>

<!-- About and Help div -->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">One Pactera Assisgnment</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li class="active" id="navbar" onclick="refreshIndex()"><a href="#">Refresh</a></li>
        <li class="active" id="navbar" onclick="showAbout()"><a href="#">About</a></li>
        <!-- <li><a href="#">Page 2</a></li> 
        <li><a href="#">Page 3</a></li>  -->
      </ul>
    </div>
  </div>
</nav>
<div id="helpBar">
<!-- <button id="About" class="btn btn-info text-right" onclick="showAbout()">About</button> -->
</div>
<!-- Main Div -->


<?php 

echo "<div id=\"getreportDiv\" class=\"centerAlign\">";
echo "<br /><button id=\"report\" class=\"btn btn-primary\" onclick=\"callReport()\">Look at Report</button>";
echo "</div>"
?>
<hr />

<div class="centerAlign" id="questions">
<dl class="dl-horizontal" >
<dt style="white-space: normal">Total Resources?</dt>
<dd><input class="form-control" type="number" id="TotalAvailableResources" oninput="calculateAvilableResources()" value="18">
<hr /></dd>

<dt style="white-space: normal">Loaned Resources?</dt>
<dd><input class="form-control" type="number" id="TotalLoaned" min="0" oninput="calculateAvilableResources()" value="0">
<hr /></dd>

<dt style="white-space: normal" >Borrowed Resources?</dt>
<dd><input class="form-control" type="number" min="0" id="TotalBorrowed" oninput="calculateAvilableResources()" value="0">
<hr /></dd>

<dt style="white-space: normal" >Available Resources?</dt>
<dd><input class="form-control" type="number" id="TotalAvailabe" value="0" readonly></dd>
</dl>
</div>
<div id="gethotfixDiv" class="centerAlign">
<button id="gethotfix" class="btn btn-primary" onclick="callGetHotfix()">Create Pactera Assignment</button> <br />
<hr />

<!-- END OF  CONTENT -->
</div>
<!-- </div> -->
<div id="contentReport" class="centerAlign"></div>
<div id="contentHotfix" class="setWidth"></div>
<div id="contentAbout" class="centerAlign"></div>
<!-- END OF BUTTONS -->

</div>


<script>$(document).ready(function(){calculateAvilableResources();});</script>
</body>
</html>
