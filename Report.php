<!-- <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

</head>
<body> -->
<script type="text/javascript">


/**
 * Ajax funtion to handle on click events on table2 
 *
 */
 //debugger
 $(document).ready(function() {
	 $("#table2 tr ").click( function(){
	
		console.log("table clicked");
		var date = $("#myDate").val();
		$.post("GenerateHTRTableWithDB.php", {selection : date }, function(data){ $("#myModal").html(data);} );
			
			
	 });
 });

function reloadReport(_option)
{
	var selectedYear = $('#reportYear').find(":selected").text();
	var select = _option + '_' + selectedYear;
	console.log(select);
	var recordsByMonth;
	$.post("GenerateReport.php", {selection : select }, function(data){

					
			if(data != "No Matches Found[]")
			{
				console.log(data);
				recordsByMonth = JSON.parse(data);
				//console.log(recordsByMonth["PTR"]);
				
				var seriesForResPerTestType = [];
				
				function generateDataForResourcesUsedPerTestType( _recordsByMonth )
				{
					var _seriesForResPerTestType = [];
					var totalPTRResources = 0;
			    	var totalFTRResources = 0;
			    	var totalHTRResources = 0;
			    	
					for(var date in _recordsByMonth["Dates"])
					{
				    	var row = $('<tr></tr>').addClass("row");
							
						
						totalPTRResources += _recordsByMonth["PTR"][date];
						totalFTRResources += _recordsByMonth["FTR"][date];
						totalHTRResources += _recordsByMonth["HTR"][date];
												
					} 
					_seriesForResPerTestType.push({ name: "Package Testing", y: totalPTRResources});
					_seriesForResPerTestType.push({ name: "Fix Verification", y: totalFTRResources});
					_seriesForResPerTestType.push({ name: "Regression Testing", y: totalHTRResources});
					return _seriesForResPerTestType;
				}

				seriesForResPerTestType = generateDataForResourcesUsedPerTestType(recordsByMonth);


			    $('#chart1').highcharts({
			        chart: {
			            type: 'pie',
			            options3d: {
			                enabled: true,
			                alpha: 45,
			                beta: 0
			            }
			        },
			        title: {
			            text: 'Monthly Resource Utilization By Test Type'
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                depth: 35,
			                dataLabels: {
			                    enabled: true,
			                    format: '{point.name}'
			                }
			            }
			        },
			        series: [{
			            colorByPoint: true,
			            name: "Testing Resources",
						data: seriesForResPerTestType
				        
			        }]
			    });
			    
				$('#table1').html(function() {
			    	
			    	var table = $('<table></table>').addClass("table table-bordered table-hover");
			    	table.append("<tr><th>Total Resources Used in " + select + "</th><th>Package Resources</th><th>Fix Resources</th><th>Regression Resources</th></tr>");
			    	//var counter=0;
	
			    	var totalPTRResources = 0;
			    	var totalFTRResources = 0;
			    	var totalHTRResources = 0;
			    	 
					for(var date in recordsByMonth["Dates"])
					{
				    	var row = $('<tr></tr>').addClass("row");
							
						totalPTRResources += recordsByMonth["PTR"][date];
						totalFTRResources += recordsByMonth["FTR"][date];
						totalHTRResources += recordsByMonth["HTR"][date];
						
						
						
					} 
					row.append("<td class=\"PTR\">" + totalPTRResources + "</td>");
					row.append("<td class=\"FTR\">" + totalFTRResources + "</td>");
					row.append("<td class=\"HTR\">" + totalHTRResources + "</td>");
					table.append(row);
			    	
					return table;
			    	
			    });


				
				//Line Chart shows the distribution of resources per day.
			    $('#chart2').highcharts({
			        chart: {
			            type: 'line'
			        },
			        title: {
			            text: 'Daily Resource utilization'
			        },
			        subtitle: {
			            text: ''
			        },
			        xAxis: {
			            categories: recordsByMonth["Dates"]
			        },
			        yAxis: {
			            title: {
			                text: 'Resources'
			            }
			        },
			        plotOptions: {
			            line: {
			                dataLabels: {
			                    enabled: true
			                },
			                enableMouseTracking: true
			            }
			        },
			        series: [{
			        	name: 'Package',
			            data: recordsByMonth["PTR"]
			        }, {
			            name: 'Regression',
			            data: recordsByMonth["HTR"]
			        }, {
			            name: 'Fix',
			            data: recordsByMonth["FTR"]
			        }]
			    });
	

				//Table shows the distribution of resources per day.
			    $('#table2').html(function() {
	
			    	var table = $('<table></table>').addClass("table table-bordered table-hover");
			    	table.append("<tr><th>Resources Per Day</th><th>Date</th><th>Package Resources</th><th>Fix Resources</th><th>Regression Resources</th></tr>");
			    	//var counter=0;
	
			    	
					for(var date in recordsByMonth["Dates"])
					{
				    	var row = $('<tr></tr>').addClass("row");
					    row.on( "click", function(){
					    		
					    		//console.log("table clicked");
					    		var date = $(this).children().eq(0).text();
					    		//console.log(date);
					    		$.post("GenerateHTRTableWithDB.php", {selection : date }, function(data){ $(".modal-body").html(data); 
					    			$("#myModal").modal('show'); 
					    			$(".modal-title").html("Detailed HTR Email Report - " + date.replace(/_/g," "));
					    			});
					    			
					    			
					    	 });
				     								
						row.append("<td class=\"myDate\" id=\"myDate\">" + recordsByMonth["Dates"][date] + "</td>");
						row.append("<td class=\"PTR\" id=\"PTR\">" + recordsByMonth["PTR"][date] + "</td>");
						row.append("<td class=\"FTR\" id=\"FTR\">" + recordsByMonth["FTR"][date] + "</td>");
						row.append("<td class=\"HTR\" id=\"HTR\">" + recordsByMonth["HTR"][date] + "</td>");
						
						table.append(row);
					} 
					
			    	
					return table;
			    	
			    });



				//TODO: Rewrite this code. Can be done better.
				//Calculations required for Chart and table to diaplay distribution of
				//resources per hotfix Type.
				
			    var typesOfHF = [];
			    var resourcesPerHFType = [];
			    
				//aggregate all the resources
				for(hotfixType in recordsByMonth["Type"])
				{
					//use this to populate types of HF
					typesOfHF.push(hotfixType);
					//HFTypes
					for(testType in recordsByMonth["Type"][hotfixType])
					{
						//console.log(resourcesPerHFType[testType.toString()]);
						if(resourcesPerHFType.hasOwnProperty(testType)) 
						{
							resourcesPerHFType[testType].push(recordsByMonth["Type"][hotfixType][testType]);
					    }
						else
						{
							//resourcesPerHFType.push(testType);
							resourcesPerHFType[testType] = [];
							//console.log(resourcesPerHFType);
							resourcesPerHFType[testType].push(recordsByMonth["Type"][hotfixType][testType]);
						}
					}
					
				}
				

				var seriesForResPerHFType = [];

				
				//function to generate the data series.
				function generateData( _typesOfHF, _resourcesPerHFType )
				{
					_series = [];
					
					for(var i=0; i<_typesOfHF.length; i++)
					{
						var totalResourcesPerTestType;
						
						for(testType in _resourcesPerHFType)
						{
							totalResourcesPerTestType = _resourcesPerHFType["PTR"][i]+_resourcesPerHFType["HTR"][i]+_resourcesPerHFType["FTR"][i] ;
							
						}
						//push the type of HF and the repective resources used.
						_series.push({name: _typesOfHF[i], y: totalResourcesPerTestType});
					}
					return _series;
				}

				//datastructure to populate the data field for Pie Chart.
				seriesForResPerHFType = generateData(typesOfHF, resourcesPerHFType);
				console.log("1." . seriesForResPerHFType);
				
				
			    $('#chart_hf_type').highcharts({
			        chart: {
			            type: 'pie',
			            options3d: {
			                enabled: true,
			                alpha: 45,
			                beta: 0
			            }
			        },
			        title: {
			            text: 'Monthly Resource Utilization By Hotfix Type'
			        },
			        tooltip: {
			            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                depth: 50,
			                dataLabels: {
			                    enabled: true,
			                    format: '{point.name}'
			                }
			            }
			        },
			        series: [{
			            colorByPoint: true,
			            name: "Testing Resources",
						data: seriesForResPerHFType
				        
			        }]
			    });
				
				//generate the table
			    $('#table_hf_type').html(function() {
	
			    	var table = $('<table></table>').addClass("table table-bordered table-hover");
			    	table.append("<tr><th>Monthly Usage</th><th>HFType</th><th>Package Resources</th><th>Fix Resources</th><th>Regression Resources</th></tr>");
			    	var counter=0;
	
			    	//in the table the coloumns are always going to be PTR,HTR and FTR
			    	//the rows will depend on the type of HFs.
					for(var typeOfHotfix in typesOfHF)
					{
				    	var row = $('<tr></tr>').addClass("row");
							
						row.append("<td class=\"hftype\">" + typesOfHF[typeOfHotfix] + "</td>");
						row.append("<td class=\"PTR\">" + resourcesPerHFType["PTR"][typeOfHotfix] + "</td>");
						row.append("<td class=\"FTR\">" + resourcesPerHFType["FTR"][typeOfHotfix] + "</td>");
						row.append("<td class=\"HTR\">" + resourcesPerHFType["HTR"][typeOfHotfix] + "</td>");
						
						table.append(row);
						counter++;
					} 
					var row_loaned = $('<tr></tr>').addClass("row");
					row_loaned.append("<td class=\"Loaned\">" + "Loaned Resources" + "</td>");
					row_loaned.append("<td class=\"Loaned\" colspan=\"3\">" + recordsByMonth["Loaned"] + "</td>");

					var row_borrowed = $('<tr></tr>').addClass("row");
					row_borrowed.append("<td class=\"Borrowed\">" + "Borrowed Resources" + "</td>");
					row_borrowed.append("<td class=\"Borrowed\" colspan=\"3\">" + recordsByMonth["Borrowed"] + "</td>");

					table.append(row_loaned);
					table.append(row_borrowed);
					
					return table;
			    	
			    });

			    
			}//close if
			else
			{	
				$('#chart1').highcharts({
					title: {
			            text: 'Sorry Data Not Present'
			        }
					 });
				$('#chart_hf_type').highcharts({
					title: {
			            text: 'Sorry Data Not Present'
			        }
					 });
				$('#chart2').highcharts({
					title: {
			            text: 'Sorry Data Not Present'
			        }
		         });
				$('#table1').html("No data found");
				$('#table2').html("No data found");
				$('#table_hf_type').html("No data found");
				console.log("reached else - No data found");
				
			}
		} 
	 );
	
}



</script>
<!-- begin bootstrap div -->
<div class="container-fluid">

<hr />
<span id=dropdown>Choose X-axis  
<!-- TODO: refactor both select staements to Jquery -->
<select id="reportYear">
	<option value="_2016" selected>2016</option>
	<option value="_2017" selected>2017</option>
</select>
<select  onchange="reloadReport(this.value)">
    <option value="Jan">January</option>
    <option value="Feb">February</option>
    <option value="Mar">March</option>
    <option value="Apr">April</option>
    <option value="May">May</option>
    <option value="Jun">June</option>
    <option value="Jul">July</option>
    <option value="Aug">August</option>
    <option value="Sep">September</option>
    <option value="Oct">October</option>
    <option value="Nov">November</option>
    <option value="Dec">December</option>
</select>
</span>

<hr />
<div id="chart1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<div id="table1"></div>
<hr style = "border-top: medium double #333;" />
<br />
<div id="chart2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<div id="table2"></div>
<hr style = "border-top: medium double #333;" />
<br />
<div id="chart_hf_type" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<div id="table_hf_type"></div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detailed HTR Email Report</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<?php
echo "<div class=\"mainbuttondiv centerAlign\">";

//echo "<input type=\"button\" class=\"btn btn-primary\" value=\"Generate Report\" id=\"reportButton\" onclick=\"reloadReport()\">";

echo "</div>";
/* echo "<div id=\"report\"> </div>"; */




?>
<!-- begin bootstrap div -->
</div>
<!--</body>
</html> -->
